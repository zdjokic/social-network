import axios from 'axios';

const config = axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}`,
});

export default config;
