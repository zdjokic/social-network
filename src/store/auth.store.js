import axios from '../config/axios';
import router from '@/router';
import { message, MessageType } from "@/utility/helpers";
import userStore from "@/store/user.store";

export default {
  namespaced: true,
  state: {
    user: null,
  },
  mutations: {
    onLoginSuccess(state, data) {
      state.user = data;
    },
    onLogout(state) {
      state.user = null;
    },
  },
  actions: {
    async register(context, payload) {
      try {
        // TODO: check if we have user with same username
        // TODO: hash password
        const { data } = await axios.post('/users', payload);
        await userStore.actions.initFollowersList(context, { id: data.id, followersList: [] });
        message(MessageType.SUCCESS, 'Successfully Registered');
        router.push('/login');
      } catch (err) {
        message(MessageType.ERROR, 'Something Went Wrong');
      }
    },
    async login(context, payload) {
      try {
        const { data } = await axios.get(
          `/users?username=${payload.username}&password=${payload.password}`
        );
        if (!data.length) {
          message(MessageType.ERROR, 'Bad Credentials');
          return;
        }
        context.commit('onLoginSuccess', data[0]);
        message(MessageType.SUCCESS, 'Successfully Logged In');
        router.push('/');
      } catch (err) {
        message(MessageType.ERROR, 'Something Went Wrong');
      }
    },
    logout(context) {
      context.commit('onLogout');
      router.push('/login');
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  }
}