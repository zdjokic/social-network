import axios from '../config/axios';
import { message, MessageType } from "@/utility/helpers";

export default {
  namespaced: true,
  state: {
    users: [],
    user: null,
    followersList: [],
  },
  mutations: {
    setUsers(state, data) {
      state.users = data;
    },
    setUser(state, data) {
      state.user = data;
    },
    setFollowersList(state, data) {
      state.followersList = data;
    },
  },
  actions: {
    async fetchUsers(context) {
      try {
        const { data } = await axios.get('/users');
        context.commit('setUsers', data);
      } catch (err) {
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async fetchUser(context, userId) {
      console.log(userId, 'user id')
      try {
        const { data } = await axios.get(`/users/${userId}`);
        context.commit('setUser', data);
      } catch (err) {
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async initFollowersList(context, payload) {
      try {
        await axios.post('/followers', payload);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async fetchFollowersList(context, userId) {
      try {
        const { data } = await axios.get(`/followers/${userId}`);
        context.commit('setFollowersList', data.followersList);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async editFollowersList(context, payload) {
      try {
        await axios.patch(`/followers/${payload.id}`, payload);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      } finally {
        await context.dispatch('fetchFollowersList', payload.id);
      }
    },
    async followUser(context, payload) {
      const apiData = {
        id: payload.id,
        followersList: [...context.state.followersList, payload.followerId]
      }
      try {
        await context.dispatch('editFollowersList', apiData);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async unfollowUser(context, payload) {
      const followersListIds = context.state.followersList.filter((id) => id !== payload.followerId);
      const apiData = {
        id: payload.id,
        followersList: followersListIds,
      }
      try {
        await context.dispatch('editFollowersList', apiData);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
  },
  getters: {
    getUsers(state) {
      return state.users;
    },
    getUser(state) {
      return state.user;
    },
    getFollowersList(state) {
      return state.followersList;
    }
  }
}