import axios from '../config/axios';
import { message, MessageType } from "@/utility/helpers";

export default {
  namespaced: true,
  state: {
    posts: [],
    comments: [],
  },
  mutations: {
    setPosts(state, data) {
      state.posts = data;
    },
    setComments(state, data) {
      state.comments = data;
    },
    onAddPost(state, data) {
      state.posts.push(data);
    },
    onAddComment(state, data) {
      state.comments.push(data);
    },
  },
  actions: {
    async fetchPosts(context) {
      try {
        const { data } = await axios.get('/posts');
        context.commit('setPosts', data);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async createPost(context, payload) {
      try {
        const { data } = await axios.post('/posts', payload);
        context.commit('onAddPost', data);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async deletePost(context, postId) {
      try {
        await axios.delete(`/posts/${postId}`);
        message(MessageType.SUCCESS, 'Successfully Deleted Post');
        context.dispatch('fetchPosts');
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async deleteComment(context, commentId) {
      try {
        await axios.delete(`/comments/${commentId}`);
        context.dispatch('fetchComments');
        message(MessageType.SUCCESS, 'Successfully Deleted Comment');
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async fetchComments(context) {
      try {
        const { data } = await axios.get('/comments');
        context.commit('setComments', data);
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async commentPost(context, payload) {
      try {
        const { data } = await axios.post('/comments', payload);
        context.commit('onAddComment', payload);
        console.log(data, 'posts');
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async editPost(context, payload) {
      try {
        const { data } = await axios.patch(`/posts/${payload.id}`, payload);
        context.dispatch('fetchPosts', data);
        message(MessageType.SUCCESS, 'Successfully Edited Post')
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
    async editComment(context, payload) {
      try {
        const { data } = await axios.patch(`/comments/${payload.id}`, payload);
        context.dispatch('fetchComments', data);
        message(MessageType.SUCCESS, 'Successfully Edited Comment');
      } catch (err) {
        console.log(err, 'error');
        message(MessageType.ERROR, 'Something went wrong');
      }
    },
  },
  getters: {
    getPosts(state) {
      return state.posts;
    },
    getComments(state) {
      return state.comments;
    },
  }
}