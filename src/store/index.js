import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

import auth from './auth.store';
import feed from './feed.store';
import user from './user.store';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    feed,
    user,
  },
  plugins: [
    createPersistedState({
      paths: [
        'auth',
      ],
    }),
  ],
})

export default store;
