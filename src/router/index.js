import Vue from 'vue';
import VueRouter from 'vue-router';
import Feed from '../views/Feed.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Feed,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue'),
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/my-profile',
    name: 'myProfile',
    component: () => import('../views/MyProfile.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/profile/:id',
    name: 'profile',
    component: () => import('../views/Profile.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/discover',
    name: 'discover',
    component: () => import('../views/Discover.vue'),
    meta: {
      requiresAuth: true
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {

  const ls = localStorage.getItem('vuex');
  const isLoggedIn = JSON.parse(ls)?.auth?.user;

  if (to.matched.some(record => record.meta.requiresAuth) && !isLoggedIn) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else next()
});

export default router;
