import Vue from 'vue';
import App from './App.vue';
import axios from './config/axios';
import './assets/tailwind.css';
import router from './router';
import store from './store';
// import helpers from "@/mixins/helpers";
import VueToast from 'vue-toast-notification';
// Import one of the available themes
//import 'vue-toast-notification/dist/theme-default.css';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);
Vue.use(require('vue-moment'));
// Vue.mixin(helpers);

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;
Vue.prototype.$message = Vue.$toast;

Vue.component('CustomButton', require('./components/UI/Button').default);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
