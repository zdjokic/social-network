import Vue from "vue";

export const MessageType = {
  SUCCESS: 'success',
  ERROR: 'error',
  INFO: 'info',
}

export const FeedType = {
  ALL: 'all',
  JUST_FOLLOWERS: 'just-followers',
}

export function message (type, message) {
  if (type === MessageType.SUCCESS) Vue.$toast.success(message);
  if (type === MessageType.ERROR) Vue.$toast.error(message);
  if (type === MessageType.INFO) Vue.$toast.info(message);
}

export function generateFakeId() {
  let result = '';
  const idLength = 5;
  const characters = '0123456789';
  const charactersLength = characters.length;

  for ( var i = 0; i < idLength; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return Number(result.toLowerCase());
}

export function sortPerNewest(arr) {
  return arr.sort((a, b) =>
    b.date > a.date ? 1 : -1
  );
}